import './styles.scss';

import * as React from 'react';

import { InitialisedPeriodical } from '../../../../../lib/interfaces/Periodical';
import { PageMetadata } from '../pageMetadata/component';
import { PeriodicalList } from '../periodicalList/component';

interface JournalIndexProps {
  periodicals: InitialisedPeriodical[];
  url: string;
}

export class JournalIndex
  extends React.Component<
  JournalIndexProps,
  {}
> {
  constructor(props: any) {
    super(props);
  }

  public render() {
    return (
      <div>
        <section className="hero is-medium is-light">
          <div className="hero-body">
            <div className="container">
              <PageMetadata
                url={this.props.url}
                title="Journals"
                description="A list of journals published on Flockademic"
              />
              <h1 className="title">Journals</h1>
            </div>
          </div>
        </section>
        <section className="section">
          <div className="container">
            <PeriodicalList periodicals={this.props.periodicals}/>
          </div>
        </section>
      </div>
    );
  }
}
